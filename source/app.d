
import std.conv;
import std.file;
import std.socket;
import std.stdio;

import args;

int PORT = 8181;
char[] HOST = "0.0.0.0".dup;

string root = "/tmp/html";

static struct AppArguments {
    @Arg("Help") bool helpWanted;
    @Arg("Host") char[] host;
    @Arg("Port") ushort port;
}

string[] splitRequestToArray(char[] request) {
    string[] results;
    int counter = 0;
    char[] word;
    foreach(char letter; request) {
	string string_letter = to!string(letter);
	if (string_letter == " ") {
	    results ~= to!string(word);
	    word = [];
	}
	else {
	    word ~= string_letter;
	}
	counter++;
    }
    return results;
}

char[] readHtmlFile(string route) {
    return cast(char[]) read(route);
}

string get200OkHeader() {
    return "HTTP/1.1 200 OK\n\n";
}

string get404NotFoundHeader() {
    return "HTTP/1.1 404 Not Found\n";
}

string get405MethodNotAllowedHeader() {
    return "HTTP/1.1 405 Method Not Allowed\n";
}

void sendMethodNotAllowed(Socket s) {
    s.send(get405MethodNotAllowedHeader() ~ "\n <h1>405 Method Not Allowed</h1>");
}

void sendNotFoundResponse(Socket s) {
    s.send(get404NotFoundHeader() ~ "\n<h1>404 Not Found</h1>");
}

void sendOkResponse(Socket s, char[] data) {
    s.send(get200OkHeader() ~ data);
}

void parseRequest(Socket s, char[] request) {
    auto split_request = splitRequestToArray(request);
    writeln(split_request);
    string requestType = split_request[0];
    string route = split_request[1];

    if (isGetRequest(requestType)) {
	if (route == "/") {
	    try {
		char[] data = readHtmlFile("/tmp/html/index.html");
		s.sendOkResponse(data);
	    } catch (FileException) {
		s.sendNotFoundResponse();
	    }
	}
	else {
	    writeln(root ~ route);
	    try {
		char[] data = readHtmlFile(root ~ route);
		s.sendOkResponse(data);
	    }
	    catch (FileException) {
		s.sendNotFoundResponse();
	    }
	}
	writeln(request);
    }
    else {
	s.sendMethodNotAllowed();
	writeln("request not implemented yet.");
    }
}

bool isGetRequest(string requestType) {

    if (requestType == "GET") {
	return true;
    }
    return false;
}

char[] setHost(char[] givenHost) {
    if (givenHost == "localhost") {
	return "127.0.0.1".dup;
    }
    else {
	return givenHost;
    }
}

void main(string[] params) {

    AppArguments p = AppArguments();
    parseArgs(p, params);
    const char[] host = setHost(p.host);

    TcpSocket s = new TcpSocket();
    s.bind(getAddress(host, p.port)[0]);
    writeln("Serving webserv on ", p.host, " at port ", p.port);
    s.listen(10);
    while(true) {
	auto connectionSocket = s.accept();
	char[1024] request;
	auto receivedBytes = connectionSocket.receive(request[]);
	parseRequest(connectionSocket, request);

	connectionSocket.close();
    }
    
}
